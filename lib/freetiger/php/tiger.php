<?php
/**
 * Copyright (c) 2012 Francisco Blas Izquierdo Riera (klondike)
 * The Tiger algorithm was written by Eli Biham and Ross Anderson and is
 * available on the official Tiger algorithm page.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    the algorithm authorsip notice, this list of conditions and the following
 *    disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. If this license is not appropriate for you please write me at
 *    klondike ( a t ) klondike ( d o t ) es to negotiate another license.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * This is an implementation of tiger made without looking at the original
 * reference code to ensure the resulting code can be published under a free
 * license. The paper was looked though to know how did tiger work.
 * It also provides the tigerp1 and tigerp2 functions.
**/

/**
 * It currently only supports 64 bit arches, 32 bit arches may be supported in
 * the future
**/

// We need uint64 arithmetic.
require_once("Uint64.php");

// S-Boxes
$tiger_t1 = array (
    Uint64::from_hex("02aab17cf7e90c5e"), Uint64::from_hex("ac424b03e243a8ec"),
    Uint64::from_hex("72cd5be30dd5fcd3"), Uint64::from_hex("6d019b93f6f97f3a"),
    Uint64::from_hex("cd9978ffd21f9193"), Uint64::from_hex("7573a1c9708029e2"),
    Uint64::from_hex("b164326b922a83c3"), Uint64::from_hex("46883eee04915870"),
    Uint64::from_hex("eaace3057103ece6"), Uint64::from_hex("c54169b808a3535c"),
    Uint64::from_hex("4ce754918ddec47c"), Uint64::from_hex("0aa2f4dfdc0df40c"),
    Uint64::from_hex("10b76f18a74dbefa"), Uint64::from_hex("c6ccb6235ad1ab6a"),
    Uint64::from_hex("13726121572fe2ff"), Uint64::from_hex("1a488c6f199d921e"),
    Uint64::from_hex("4bc9f9f4da0007ca"), Uint64::from_hex("26f5e6f6e85241c7"),
    Uint64::from_hex("859079dbea5947b6"), Uint64::from_hex("4f1885c5c99e8c92"),
    Uint64::from_hex("d78e761ea96f864b"), Uint64::from_hex("8e36428c52b5c17d"),
    Uint64::from_hex("69cf6827373063c1"), Uint64::from_hex("b607c93d9bb4c56e"),
    Uint64::from_hex("7d820e760e76b5ea"), Uint64::from_hex("645c9cc6f07fdc42"),
    Uint64::from_hex("bf38a078243342e0"), Uint64::from_hex("5f6b343c9d2e7d04"),
    Uint64::from_hex("f2c28aeb600b0ec6"), Uint64::from_hex("6c0ed85f7254bcac"),
    Uint64::from_hex("71592281a4db4fe5"), Uint64::from_hex("1967fa69ce0fed9f"),
    Uint64::from_hex("fd5293f8b96545db"), Uint64::from_hex("c879e9d7f2a7600b"),
    Uint64::from_hex("860248920193194e"), Uint64::from_hex("a4f9533b2d9cc0b3"),
    Uint64::from_hex("9053836c15957613"), Uint64::from_hex("db6dcf8afc357bf1"),
    Uint64::from_hex("18beea7a7a370f57"), Uint64::from_hex("037117ca50b99066"),
    Uint64::from_hex("6ab30a9774424a35"), Uint64::from_hex("f4e92f02e325249b"),
    Uint64::from_hex("7739db07061ccae1"), Uint64::from_hex("d8f3b49ceca42a05"),
    Uint64::from_hex("bd56be3f51382f73"), Uint64::from_hex("45faed5843b0bb28"),
    Uint64::from_hex("1c813d5c11bf1f83"), Uint64::from_hex("8af0e4b6d75fa169"),
    Uint64::from_hex("33ee18a487ad9999"), Uint64::from_hex("3c26e8eab1c94410"),
    Uint64::from_hex("b510102bc0a822f9"), Uint64::from_hex("141eef310ce6123b"),
    Uint64::from_hex("fc65b90059ddb154"), Uint64::from_hex("e0158640c5e0e607"),
    Uint64::from_hex("884e079826c3a3cf"), Uint64::from_hex("930d0d9523c535fd"),
    Uint64::from_hex("35638d754e9a2b00"), Uint64::from_hex("4085fccf40469dd5"),
    Uint64::from_hex("c4b17ad28be23a4c"), Uint64::from_hex("cab2f0fc6a3e6a2e"),
    Uint64::from_hex("2860971a6b943fcd"), Uint64::from_hex("3dde6ee212e30446"),
    Uint64::from_hex("6222f32ae01765ae"), Uint64::from_hex("5d550bb5478308fe"),
    Uint64::from_hex("a9efa98da0eda22a"), Uint64::from_hex("c351a71686c40da7"),
    Uint64::from_hex("1105586d9c867c84"), Uint64::from_hex("dcffee85fda22853"),
    Uint64::from_hex("ccfbd0262c5eef76"), Uint64::from_hex("baf294cb8990d201"),
    Uint64::from_hex("e69464f52afad975"), Uint64::from_hex("94b013afdf133e14"),
    Uint64::from_hex("06a7d1a32823c958"), Uint64::from_hex("6f95fe5130f61119"),
    Uint64::from_hex("d92ab34e462c06c0"), Uint64::from_hex("ed7bde33887c71d2"),
    Uint64::from_hex("79746d6e6518393e"), Uint64::from_hex("5ba419385d713329"),
    Uint64::from_hex("7c1ba6b948a97564"), Uint64::from_hex("31987c197bfdac67"),
    Uint64::from_hex("de6c23c44b053d02"), Uint64::from_hex("581c49fed002d64d"),
    Uint64::from_hex("dd474d6338261571"), Uint64::from_hex("aa4546c3e473d062"),
    Uint64::from_hex("928fce349455f860"), Uint64::from_hex("48161bbacaab94d9"),
    Uint64::from_hex("63912430770e6f68"), Uint64::from_hex("6ec8a5e602c6641c"),
    Uint64::from_hex("87282515337ddd2b"), Uint64::from_hex("2cda6b42034b701b"),
    Uint64::from_hex("b03d37c181cb096d"), Uint64::from_hex("e108438266c71c6f"),
    Uint64::from_hex("2b3180c7eb51b255"), Uint64::from_hex("df92b82f96c08bbc"),
    Uint64::from_hex("5c68c8c0a632f3ba"), Uint64::from_hex("5504cc861c3d0556"),
    Uint64::from_hex("abbfa4e55fb26b8f"), Uint64::from_hex("41848b0ab3baceb4"),
    Uint64::from_hex("b334a273aa445d32"), Uint64::from_hex("bca696f0a85ad881"),
    Uint64::from_hex("24f6ec65b528d56c"), Uint64::from_hex("0ce1512e90f4524a"),
    Uint64::from_hex("4e9dd79d5506d35a"), Uint64::from_hex("258905fac6ce9779"),
    Uint64::from_hex("2019295b3e109b33"), Uint64::from_hex("f8a9478b73a054cc"),
    Uint64::from_hex("2924f2f934417eb0"), Uint64::from_hex("3993357d536d1bc4"),
    Uint64::from_hex("38a81ac21db6ff8b"), Uint64::from_hex("47c4fbf17d6016bf"),
    Uint64::from_hex("1e0faadd7667e3f5"), Uint64::from_hex("7abcff62938beb96"),
    Uint64::from_hex("a78dad948fc179c9"), Uint64::from_hex("8f1f98b72911e50d"),
    Uint64::from_hex("61e48eae27121a91"), Uint64::from_hex("4d62f7ad31859808"),
    Uint64::from_hex("eceba345ef5ceaeb"), Uint64::from_hex("f5ceb25ebc9684ce"),
    Uint64::from_hex("f633e20cb7f76221"), Uint64::from_hex("a32cdf06ab8293e4"),
    Uint64::from_hex("985a202ca5ee2ca4"), Uint64::from_hex("cf0b8447cc8a8fb1"),
    Uint64::from_hex("9f765244979859a3"), Uint64::from_hex("a8d516b1a1240017"),
    Uint64::from_hex("0bd7ba3ebb5dc726"), Uint64::from_hex("e54bca55b86adb39"),
    Uint64::from_hex("1d7a3afd6c478063"), Uint64::from_hex("519ec608e7669edd"),
    Uint64::from_hex("0e5715a2d149aa23"), Uint64::from_hex("177d4571848ff194"),
    Uint64::from_hex("eeb55f3241014c22"), Uint64::from_hex("0f5e5ca13a6e2ec2"),
    Uint64::from_hex("8029927b75f5c361"), Uint64::from_hex("ad139fabc3d6e436"),
    Uint64::from_hex("0d5df1a94ccf402f"), Uint64::from_hex("3e8bd948bea5dfc8"),
    Uint64::from_hex("a5a0d357bd3ff77e"), Uint64::from_hex("a2d12e251f74f645"),
    Uint64::from_hex("66fd9e525e81a082"), Uint64::from_hex("2e0c90ce7f687a49"),
    Uint64::from_hex("c2e8bcbeba973bc5"), Uint64::from_hex("000001bce509745f"),
    Uint64::from_hex("423777bbe6dab3d6"), Uint64::from_hex("d1661c7eaef06eb5"),
    Uint64::from_hex("a1781f354daacfd8"), Uint64::from_hex("2d11284a2b16affc"),
    Uint64::from_hex("f1fc4f67fa891d1f"), Uint64::from_hex("73ecc25dcb920ada"),
    Uint64::from_hex("ae610c22c2a12651"), Uint64::from_hex("96e0a810d356b78a"),
    Uint64::from_hex("5a9a381f2fe7870f"), Uint64::from_hex("d5ad62ede94e5530"),
    Uint64::from_hex("d225e5e8368d1427"), Uint64::from_hex("65977b70c7af4631"),
    Uint64::from_hex("99f889b2de39d74f"), Uint64::from_hex("233f30bf54e1d143"),
    Uint64::from_hex("9a9675d3d9a63c97"), Uint64::from_hex("5470554ff334f9a8"),
    Uint64::from_hex("166acb744a4f5688"), Uint64::from_hex("70c74caab2e4aead"),
    Uint64::from_hex("f0d091646f294d12"), Uint64::from_hex("57b82a89684031d1"),
    Uint64::from_hex("efd95a5a61be0b6b"), Uint64::from_hex("2fbd12e969f2f29a"),
    Uint64::from_hex("9bd37013feff9fe8"), Uint64::from_hex("3f9b0404d6085a06"),
    Uint64::from_hex("4940c1f3166cfe15"), Uint64::from_hex("09542c4dcdf3defb"),
    Uint64::from_hex("b4c5218385cd5ce3"), Uint64::from_hex("c935b7dc4462a641"),
    Uint64::from_hex("3417f8a68ed3b63f"), Uint64::from_hex("b80959295b215b40"),
    Uint64::from_hex("f99cdaef3b8c8572"), Uint64::from_hex("018c0614f8fcb95d"),
    Uint64::from_hex("1b14accd1a3acdf3"), Uint64::from_hex("84d471f200bb732d"),
    Uint64::from_hex("c1a3110e95e8da16"), Uint64::from_hex("430a7220bf1a82b8"),
    Uint64::from_hex("b77e090d39df210e"), Uint64::from_hex("5ef4bd9f3cd05e9d"),
    Uint64::from_hex("9d4ff6da7e57a444"), Uint64::from_hex("da1d60e183d4a5f8"),
    Uint64::from_hex("b287c38417998e47"), Uint64::from_hex("fe3edc121bb31886"),
    Uint64::from_hex("c7fe3ccc980ccbef"), Uint64::from_hex("e46fb590189bfd03"),
    Uint64::from_hex("3732fd469a4c57dc"), Uint64::from_hex("7ef700a07cf1ad65"),
    Uint64::from_hex("59c64468a31d8859"), Uint64::from_hex("762fb0b4d45b61f6"),
    Uint64::from_hex("155baed099047718"), Uint64::from_hex("68755e4c3d50baa6"),
    Uint64::from_hex("e9214e7f22d8b4df"), Uint64::from_hex("2addbf532eac95f4"),
    Uint64::from_hex("32ae3909b4bd0109"), Uint64::from_hex("834df537b08e3450"),
    Uint64::from_hex("fa209da84220728d"), Uint64::from_hex("9e691d9b9efe23f7"),
    Uint64::from_hex("0446d288c4ae8d7f"), Uint64::from_hex("7b4cc524e169785b"),
    Uint64::from_hex("21d87f0135ca1385"), Uint64::from_hex("cebb400f137b8aa5"),
    Uint64::from_hex("272e2b66580796be"), Uint64::from_hex("3612264125c2b0de"),
    Uint64::from_hex("057702bdad1efbb2"), Uint64::from_hex("d4babb8eacf84be9"),
    Uint64::from_hex("91583139641bc67b"), Uint64::from_hex("8bdc2de08036e024"),
    Uint64::from_hex("603c8156f49f68ed"), Uint64::from_hex("f7d236f7dbef5111"),
    Uint64::from_hex("9727c4598ad21e80"), Uint64::from_hex("a08a0896670a5fd7"),
    Uint64::from_hex("cb4a8f4309eba9cb"), Uint64::from_hex("81af564b0f7036a1"),
    Uint64::from_hex("c0b99aa778199abd"), Uint64::from_hex("959f1ec83fc8e952"),
    Uint64::from_hex("8c505077794a81b9"), Uint64::from_hex("3acaaf8f056338f0"),
    Uint64::from_hex("07b43f50627a6778"), Uint64::from_hex("4a44ab49f5eccc77"),
    Uint64::from_hex("3bc3d6e4b679ee98"), Uint64::from_hex("9cc0d4d1cf14108c"),
    Uint64::from_hex("4406c00b206bc8a0"), Uint64::from_hex("82a18854c8d72d89"),
    Uint64::from_hex("67e366b35c3c432c"), Uint64::from_hex("b923dd61102b37f2"),
    Uint64::from_hex("56ab2779d884271d"), Uint64::from_hex("be83e1b0ff1525af"),
    Uint64::from_hex("fb7c65d4217e49a9"), Uint64::from_hex("6bdbe0e76d48e7d4"),
    Uint64::from_hex("08df828745d9179e"), Uint64::from_hex("22ea6a9add53bd34"),
    Uint64::from_hex("e36e141c5622200a"), Uint64::from_hex("7f805d1b8cb750ee"),
    Uint64::from_hex("afe5c7a59f58e837"), Uint64::from_hex("e27f996a4fb1c23c"),
    Uint64::from_hex("d3867dfb0775f0d0"), Uint64::from_hex("d0e673de6e88891a"),
    Uint64::from_hex("123aeb9eafb86c25"), Uint64::from_hex("30f1d5d5c145b895"),
    Uint64::from_hex("bb434a2dee7269e7"), Uint64::from_hex("78cb67ecf931fa38"),
    Uint64::from_hex("f33b0372323bbf9c"), Uint64::from_hex("52d66336fb279c74"),
    Uint64::from_hex("505f33ac0afb4eaa"), Uint64::from_hex("e8a5cd99a2cce187"),
    Uint64::from_hex("534974801e2d30bb"), Uint64::from_hex("8d2d5711d5876d90"),
    Uint64::from_hex("1f1a412891bc038e"), Uint64::from_hex("d6e2e71d82e56648"),
    Uint64::from_hex("74036c3a497732b7"), Uint64::from_hex("89b67ed96361f5ab"),
    Uint64::from_hex("ffed95d8f1ea02a2"), Uint64::from_hex("e72b3bd61464d43d"),
    Uint64::from_hex("a6300f170bdc4820"), Uint64::from_hex("ebc18760ed78a77a")
);

$tiger_t2 = array (
    Uint64::from_hex("e6a6be5a05a12138"), Uint64::from_hex("b5a122a5b4f87c98"),
    Uint64::from_hex("563c6089140b6990"), Uint64::from_hex("4c46cb2e391f5dd5"),
    Uint64::from_hex("d932addbc9b79434"), Uint64::from_hex("08ea70e42015aff5"),
    Uint64::from_hex("d765a6673e478cf1"), Uint64::from_hex("c4fb757eab278d99"),
    Uint64::from_hex("df11c6862d6e0692"), Uint64::from_hex("ddeb84f10d7f3b16"),
    Uint64::from_hex("6f2ef604a665ea04"), Uint64::from_hex("4a8e0f0ff0e0dfb3"),
    Uint64::from_hex("a5edeef83dbcba51"), Uint64::from_hex("fc4f0a2a0ea4371e"),
    Uint64::from_hex("e83e1da85cb38429"), Uint64::from_hex("dc8ff882ba1b1ce2"),
    Uint64::from_hex("cd45505e8353e80d"), Uint64::from_hex("18d19a00d4db0717"),
    Uint64::from_hex("34a0cfeda5f38101"), Uint64::from_hex("0be77e518887caf2"),
    Uint64::from_hex("1e341438b3c45136"), Uint64::from_hex("e05797f49089ccf9"),
    Uint64::from_hex("ffd23f9df2591d14"), Uint64::from_hex("543dda228595c5cd"),
    Uint64::from_hex("661f81fd99052a33"), Uint64::from_hex("8736e641db0f7b76"),
    Uint64::from_hex("15227725418e5307"), Uint64::from_hex("e25f7f46162eb2fa"),
    Uint64::from_hex("48a8b2126c13d9fe"), Uint64::from_hex("afdc541792e76eea"),
    Uint64::from_hex("03d912bfc6d1898f"), Uint64::from_hex("31b1aafa1b83f51b"),
    Uint64::from_hex("f1ac2796e42ab7d9"), Uint64::from_hex("40a3a7d7fcd2ebac"),
    Uint64::from_hex("1056136d0afbbcc5"), Uint64::from_hex("7889e1dd9a6d0c85"),
    Uint64::from_hex("d33525782a7974aa"), Uint64::from_hex("a7e25d09078ac09b"),
    Uint64::from_hex("bd4138b3eac6edd0"), Uint64::from_hex("920abfbe71eb9e70"),
    Uint64::from_hex("a2a5d0f54fc2625c"), Uint64::from_hex("c054e36b0b1290a3"),
    Uint64::from_hex("f6dd59ff62fe932b"), Uint64::from_hex("3537354511a8ac7d"),
    Uint64::from_hex("ca845e9172fadcd4"), Uint64::from_hex("84f82b60329d20dc"),
    Uint64::from_hex("79c62ce1cd672f18"), Uint64::from_hex("8b09a2add124642c"),
    Uint64::from_hex("d0c1e96a19d9e726"), Uint64::from_hex("5a786a9b4ba9500c"),
    Uint64::from_hex("0e020336634c43f3"), Uint64::from_hex("c17b474aeb66d822"),
    Uint64::from_hex("6a731ae3ec9baac2"), Uint64::from_hex("8226667ae0840258"),
    Uint64::from_hex("67d4567691caeca5"), Uint64::from_hex("1d94155c4875adb5"),
    Uint64::from_hex("6d00fd985b813fdf"), Uint64::from_hex("51286efcb774cd06"),
    Uint64::from_hex("5e8834471fa744af"), Uint64::from_hex("f72ca0aee761ae2e"),
    Uint64::from_hex("be40e4cdaee8e09a"), Uint64::from_hex("e9970bbb5118f665"),
    Uint64::from_hex("726e4beb33df1964"), Uint64::from_hex("703b000729199762"),
    Uint64::from_hex("4631d816f5ef30a7"), Uint64::from_hex("b880b5b51504a6be"),
    Uint64::from_hex("641793c37ed84b6c"), Uint64::from_hex("7b21ed77f6e97d96"),
    Uint64::from_hex("776306312ef96b73"), Uint64::from_hex("ae528948e86ff3f4"),
    Uint64::from_hex("53dbd7f286a3f8f8"), Uint64::from_hex("16cadce74cfc1063"),
    Uint64::from_hex("005c19bdfa52c6dd"), Uint64::from_hex("68868f5d64d46ad3"),
    Uint64::from_hex("3a9d512ccf1e186a"), Uint64::from_hex("367e62c2385660ae"),
    Uint64::from_hex("e359e7ea77dcb1d7"), Uint64::from_hex("526c0773749abe6e"),
    Uint64::from_hex("735ae5f9d09f734b"), Uint64::from_hex("493fc7cc8a558ba8"),
    Uint64::from_hex("b0b9c1533041ab45"), Uint64::from_hex("321958ba470a59bd"),
    Uint64::from_hex("852db00b5f46c393"), Uint64::from_hex("91209b2bd336b0e5"),
    Uint64::from_hex("6e604f7d659ef19f"), Uint64::from_hex("b99a8ae2782ccb24"),
    Uint64::from_hex("ccf52ab6c814c4c7"), Uint64::from_hex("4727d9afbe11727b"),
    Uint64::from_hex("7e950d0c0121b34d"), Uint64::from_hex("756f435670ad471f"),
    Uint64::from_hex("f5add442615a6849"), Uint64::from_hex("4e87e09980b9957a"),
    Uint64::from_hex("2acfa1df50aee355"), Uint64::from_hex("d898263afd2fd556"),
    Uint64::from_hex("c8f4924dd80c8fd6"), Uint64::from_hex("cf99ca3d754a173a"),
    Uint64::from_hex("fe477bacaf91bf3c"), Uint64::from_hex("ed5371f6d690c12d"),
    Uint64::from_hex("831a5c285e687094"), Uint64::from_hex("c5d3c90a3708a0a4"),
    Uint64::from_hex("0f7f903717d06580"), Uint64::from_hex("19f9bb13b8fdf27f"),
    Uint64::from_hex("b1bd6f1b4d502843"), Uint64::from_hex("1c761ba38fff4012"),
    Uint64::from_hex("0d1530c4e2e21f3b"), Uint64::from_hex("8943ce69a7372c8a"),
    Uint64::from_hex("e5184e11feb5ce66"), Uint64::from_hex("618bdb80bd736621"),
    Uint64::from_hex("7d29bad68b574d0b"), Uint64::from_hex("81bb613e25e6fe5b"),
    Uint64::from_hex("071c9c10bc07913f"), Uint64::from_hex("c7beeb7909ac2d97"),
    Uint64::from_hex("c3e58d353bc5d757"), Uint64::from_hex("eb017892f38f61e8"),
    Uint64::from_hex("d4effb9c9b1cc21a"), Uint64::from_hex("99727d26f494f7ab"),
    Uint64::from_hex("a3e063a2956b3e03"), Uint64::from_hex("9d4a8b9a4aa09c30"),
    Uint64::from_hex("3f6ab7d500090fb4"), Uint64::from_hex("9cc0f2a057268ac0"),
    Uint64::from_hex("3dee9d2dedbf42d1"), Uint64::from_hex("330f49c87960a972"),
    Uint64::from_hex("c6b2720287421b41"), Uint64::from_hex("0ac59ec07c00369c"),
    Uint64::from_hex("ef4eac49cb353425"), Uint64::from_hex("f450244eef0129d8"),
    Uint64::from_hex("8acc46e5caf4deb6"), Uint64::from_hex("2ffeab63989263f7"),
    Uint64::from_hex("8f7cb9fe5d7a4578"), Uint64::from_hex("5bd8f7644e634635"),
    Uint64::from_hex("427a7315bf2dc900"), Uint64::from_hex("17d0c4aa2125261c"),
    Uint64::from_hex("3992486c93518e50"), Uint64::from_hex("b4cbfee0a2d7d4c3"),
    Uint64::from_hex("7c75d6202c5ddd8d"), Uint64::from_hex("dbc295d8e35b6c61"),
    Uint64::from_hex("60b369d302032b19"), Uint64::from_hex("ce42685fdce44132"),
    Uint64::from_hex("06f3ddb9ddf65610"), Uint64::from_hex("8ea4d21db5e148f0"),
    Uint64::from_hex("20b0fce62fcd496f"), Uint64::from_hex("2c1b912358b0ee31"),
    Uint64::from_hex("b28317b818f5a308"), Uint64::from_hex("a89c1e189ca6d2cf"),
    Uint64::from_hex("0c6b18576aaadbc8"), Uint64::from_hex("b65deaa91299fae3"),
    Uint64::from_hex("fb2b794b7f1027e7"), Uint64::from_hex("04e4317f443b5beb"),
    Uint64::from_hex("4b852d325939d0a6"), Uint64::from_hex("d5ae6beefb207ffc"),
    Uint64::from_hex("309682b281c7d374"), Uint64::from_hex("bae309a194c3b475"),
    Uint64::from_hex("8cc3f97b13b49f05"), Uint64::from_hex("98a9422ff8293967"),
    Uint64::from_hex("244b16b01076ff7c"), Uint64::from_hex("f8bf571c663d67ee"),
    Uint64::from_hex("1f0d6758eee30da1"), Uint64::from_hex("c9b611d97adeb9b7"),
    Uint64::from_hex("b7afd5887b6c57a2"), Uint64::from_hex("6290ae846b984fe1"),
    Uint64::from_hex("94df4cdeacc1a5fd"), Uint64::from_hex("058a5bd1c5483aff"),
    Uint64::from_hex("63166cc142ba3c37"), Uint64::from_hex("8db8526eb2f76f40"),
    Uint64::from_hex("e10880036f0d6d4e"), Uint64::from_hex("9e0523c9971d311d"),
    Uint64::from_hex("45ec2824cc7cd691"), Uint64::from_hex("575b8359e62382c9"),
    Uint64::from_hex("fa9e400dc4889995"), Uint64::from_hex("d1823ecb45721568"),
    Uint64::from_hex("dafd983b8206082f"), Uint64::from_hex("aa7d29082386a8cb"),
    Uint64::from_hex("269fcd4403b87588"), Uint64::from_hex("1b91f5f728bdd1e0"),
    Uint64::from_hex("e4669f39040201f6"), Uint64::from_hex("7a1d7c218cf04ade"),
    Uint64::from_hex("65623c29d79ce5ce"), Uint64::from_hex("2368449096c00bb1"),
    Uint64::from_hex("ab9bf1879da503ba"), Uint64::from_hex("bc23ecb1a458058e"),
    Uint64::from_hex("9a58df01bb401ecc"), Uint64::from_hex("a070e868a85f143d"),
    Uint64::from_hex("4ff188307df2239e"), Uint64::from_hex("14d565b41a641183"),
    Uint64::from_hex("ee13337452701602"), Uint64::from_hex("950e3dcf3f285e09"),
    Uint64::from_hex("59930254b9c80953"), Uint64::from_hex("3bf299408930da6d"),
    Uint64::from_hex("a955943f53691387"), Uint64::from_hex("a15edecaa9cb8784"),
    Uint64::from_hex("29142127352be9a0"), Uint64::from_hex("76f0371fff4e7afb"),
    Uint64::from_hex("0239f450274f2228"), Uint64::from_hex("bb073af01d5e868b"),
    Uint64::from_hex("bfc80571c10e96c1"), Uint64::from_hex("d267088568222e23"),
    Uint64::from_hex("9671a3d48e80b5b0"), Uint64::from_hex("55b5d38ae193bb81"),
    Uint64::from_hex("693ae2d0a18b04b8"), Uint64::from_hex("5c48b4ecadd5335f"),
    Uint64::from_hex("fd743b194916a1ca"), Uint64::from_hex("2577018134be98c4"),
    Uint64::from_hex("e77987e83c54a4ad"), Uint64::from_hex("28e11014da33e1b9"),
    Uint64::from_hex("270cc59e226aa213"), Uint64::from_hex("71495f756d1a5f60"),
    Uint64::from_hex("9be853fb60afef77"), Uint64::from_hex("adc786a7f7443dbf"),
    Uint64::from_hex("0904456173b29a82"), Uint64::from_hex("58bc7a66c232bd5e"),
    Uint64::from_hex("f306558c673ac8b2"), Uint64::from_hex("41f639c6b6c9772a"),
    Uint64::from_hex("216defe99fda35da"), Uint64::from_hex("11640cc71c7be615"),
    Uint64::from_hex("93c43694565c5527"), Uint64::from_hex("ea038e6246777839"),
    Uint64::from_hex("f9abf3ce5a3e2469"), Uint64::from_hex("741e768d0fd312d2"),
    Uint64::from_hex("0144b883ced652c6"), Uint64::from_hex("c20b5a5ba33f8552"),
    Uint64::from_hex("1ae69633c3435a9d"), Uint64::from_hex("97a28ca4088cfdec"),
    Uint64::from_hex("8824a43c1e96f420"), Uint64::from_hex("37612fa66eeea746"),
    Uint64::from_hex("6b4cb165f9cf0e5a"), Uint64::from_hex("43aa1c06a0abfb4a"),
    Uint64::from_hex("7f4dc26ff162796b"), Uint64::from_hex("6cbacc8e54ed9b0f"),
    Uint64::from_hex("a6b7ffefd2bb253e"), Uint64::from_hex("2e25bc95b0a29d4f"),
    Uint64::from_hex("86d6a58bdef1388c"), Uint64::from_hex("ded74ac576b6f054"),
    Uint64::from_hex("8030bdbc2b45805d"), Uint64::from_hex("3c81af70e94d9289"),
    Uint64::from_hex("3eff6dda9e3100db"), Uint64::from_hex("b38dc39fdfcc8847"),
    Uint64::from_hex("123885528d17b87e"), Uint64::from_hex("f2da0ed240b1b642"),
    Uint64::from_hex("44cefadcd54bf9a9"), Uint64::from_hex("1312200e433c7ee6"),
    Uint64::from_hex("9ffcc84f3a78c748"), Uint64::from_hex("f0cd1f72248576bb"),
    Uint64::from_hex("ec6974053638cfe4"), Uint64::from_hex("2ba7b67c0cec4e4c"),
    Uint64::from_hex("ac2f4df3e5ce32ed"), Uint64::from_hex("cb33d14326ea4c11"),
    Uint64::from_hex("a4e9044cc77e58bc"), Uint64::from_hex("5f513293d934fcef"),
    Uint64::from_hex("5dc9645506e55444"), Uint64::from_hex("50de418f317de40a"),
    Uint64::from_hex("388cb31a69dde259"), Uint64::from_hex("2db4a83455820a86"),
    Uint64::from_hex("9010a91e84711ae9"), Uint64::from_hex("4df7f0b7b1498371"),
    Uint64::from_hex("d62a2eabc0977179"), Uint64::from_hex("22fac097aa8d5c0e")
);

$tiger_t3 = array (
    Uint64::from_hex("f49fcc2ff1daf39b"), Uint64::from_hex("487fd5c66ff29281"),
    Uint64::from_hex("e8a30667fcdca83f"), Uint64::from_hex("2c9b4be3d2fcce63"),
    Uint64::from_hex("da3ff74b93fbbbc2"), Uint64::from_hex("2fa165d2fe70ba66"),
    Uint64::from_hex("a103e279970e93d4"), Uint64::from_hex("becdec77b0e45e71"),
    Uint64::from_hex("cfb41e723985e497"), Uint64::from_hex("b70aaa025ef75017"),
    Uint64::from_hex("d42309f03840b8e0"), Uint64::from_hex("8efc1ad035898579"),
    Uint64::from_hex("96c6920be2b2abc5"), Uint64::from_hex("66af4163375a9172"),
    Uint64::from_hex("2174abdcca7127fb"), Uint64::from_hex("b33ccea64a72ff41"),
    Uint64::from_hex("f04a4933083066a5"), Uint64::from_hex("8d970acdd7289af5"),
    Uint64::from_hex("8f96e8e031c8c25e"), Uint64::from_hex("f3fec02276875d47"),
    Uint64::from_hex("ec7bf310056190dd"), Uint64::from_hex("f5adb0aebb0f1491"),
    Uint64::from_hex("9b50f8850fd58892"), Uint64::from_hex("4975488358b74de8"),
    Uint64::from_hex("a3354ff691531c61"), Uint64::from_hex("0702bbe481d2c6ee"),
    Uint64::from_hex("89fb24057deded98"), Uint64::from_hex("ac3075138596e902"),
    Uint64::from_hex("1d2d3580172772ed"), Uint64::from_hex("eb738fc28e6bc30d"),
    Uint64::from_hex("5854ef8f63044326"), Uint64::from_hex("9e5c52325add3bbe"),
    Uint64::from_hex("90aa53cf325c4623"), Uint64::from_hex("c1d24d51349dd067"),
    Uint64::from_hex("2051cfeea69ea624"), Uint64::from_hex("13220f0a862e7e4f"),
    Uint64::from_hex("ce39399404e04864"), Uint64::from_hex("d9c42ca47086fcb7"),
    Uint64::from_hex("685ad2238a03e7cc"), Uint64::from_hex("066484b2ab2ff1db"),
    Uint64::from_hex("fe9d5d70efbf79ec"), Uint64::from_hex("5b13b9dd9c481854"),
    Uint64::from_hex("15f0d475ed1509ad"), Uint64::from_hex("0bebcd060ec79851"),
    Uint64::from_hex("d58c6791183ab7f8"), Uint64::from_hex("d1187c5052f3eee4"),
    Uint64::from_hex("c95d1192e54e82ff"), Uint64::from_hex("86eea14cb9ac6ca2"),
    Uint64::from_hex("3485beb153677d5d"), Uint64::from_hex("dd191d781f8c492a"),
    Uint64::from_hex("f60866baa784ebf9"), Uint64::from_hex("518f643ba2d08c74"),
    Uint64::from_hex("8852e956e1087c22"), Uint64::from_hex("a768cb8dc410ae8d"),
    Uint64::from_hex("38047726bfec8e1a"), Uint64::from_hex("a67738b4cd3b45aa"),
    Uint64::from_hex("ad16691cec0dde19"), Uint64::from_hex("c6d4319380462e07"),
    Uint64::from_hex("c5a5876d0ba61938"), Uint64::from_hex("16b9fa1fa58fd840"),
    Uint64::from_hex("188ab1173ca74f18"), Uint64::from_hex("abda2f98c99c021f"),
    Uint64::from_hex("3e0580ab134ae816"), Uint64::from_hex("5f3b05b773645abb"),
    Uint64::from_hex("2501a2be5575f2f6"), Uint64::from_hex("1b2f74004e7e8ba9"),
    Uint64::from_hex("1cd7580371e8d953"), Uint64::from_hex("7f6ed89562764e30"),
    Uint64::from_hex("b15926ff596f003d"), Uint64::from_hex("9f65293da8c5d6b9"),
    Uint64::from_hex("6ecef04dd690f84c"), Uint64::from_hex("4782275fff33af88"),
    Uint64::from_hex("e41433083f820801"), Uint64::from_hex("fd0dfe409a1af9b5"),
    Uint64::from_hex("4325a3342cdb396b"), Uint64::from_hex("8ae77e62b301b252"),
    Uint64::from_hex("c36f9e9f6655615a"), Uint64::from_hex("85455a2d92d32c09"),
    Uint64::from_hex("f2c7dea949477485"), Uint64::from_hex("63cfb4c133a39eba"),
    Uint64::from_hex("83b040cc6ebc5462"), Uint64::from_hex("3b9454c8fdb326b0"),
    Uint64::from_hex("56f56a9e87ffd78c"), Uint64::from_hex("2dc2940d99f42bc6"),
    Uint64::from_hex("98f7df096b096e2d"), Uint64::from_hex("19a6e01e3ad852bf"),
    Uint64::from_hex("42a99ccbdbd4b40b"), Uint64::from_hex("a59998af45e9c559"),
    Uint64::from_hex("366295e807d93186"), Uint64::from_hex("6b48181bfaa1f773"),
    Uint64::from_hex("1fec57e2157a0a1d"), Uint64::from_hex("4667446af6201ad5"),
    Uint64::from_hex("e615ebcacfb0f075"), Uint64::from_hex("b8f31f4f68290778"),
    Uint64::from_hex("22713ed6ce22d11e"), Uint64::from_hex("3057c1a72ec3c93b"),
    Uint64::from_hex("cb46acc37c3f1f2f"), Uint64::from_hex("dbb893fd02aaf50e"),
    Uint64::from_hex("331fd92e600b9fcf"), Uint64::from_hex("a498f96148ea3ad6"),
    Uint64::from_hex("a8d8426e8b6a83ea"), Uint64::from_hex("a089b274b7735cdc"),
    Uint64::from_hex("87f6b3731e524a11"), Uint64::from_hex("118808e5cbc96749"),
    Uint64::from_hex("9906e4c7b19bd394"), Uint64::from_hex("afed7f7e9b24a20c"),
    Uint64::from_hex("6509eadeeb3644a7"), Uint64::from_hex("6c1ef1d3e8ef0ede"),
    Uint64::from_hex("b9c97d43e9798fb4"), Uint64::from_hex("a2f2d784740c28a3"),
    Uint64::from_hex("7b8496476197566f"), Uint64::from_hex("7a5be3e6b65f069d"),
    Uint64::from_hex("f96330ed78be6f10"), Uint64::from_hex("eee60de77a076a15"),
    Uint64::from_hex("2b4bee4aa08b9bd0"), Uint64::from_hex("6a56a63ec7b8894e"),
    Uint64::from_hex("02121359ba34fef4"), Uint64::from_hex("4cbf99f8283703fc"),
    Uint64::from_hex("398071350caf30c8"), Uint64::from_hex("d0a77a89f017687a"),
    Uint64::from_hex("f1c1a9eb9e423569"), Uint64::from_hex("8c7976282dee8199"),
    Uint64::from_hex("5d1737a5dd1f7abd"), Uint64::from_hex("4f53433c09a9fa80"),
    Uint64::from_hex("fa8b0c53df7ca1d9"), Uint64::from_hex("3fd9dcbc886ccb77"),
    Uint64::from_hex("c040917ca91b4720"), Uint64::from_hex("7dd00142f9d1dcdf"),
    Uint64::from_hex("8476fc1d4f387b58"), Uint64::from_hex("23f8e7c5f3316503"),
    Uint64::from_hex("032a2244e7e37339"), Uint64::from_hex("5c87a5d750f5a74b"),
    Uint64::from_hex("082b4cc43698992e"), Uint64::from_hex("df917becb858f63c"),
    Uint64::from_hex("3270b8fc5bf86dda"), Uint64::from_hex("10ae72bb29b5dd76"),
    Uint64::from_hex("576ac94e7700362b"), Uint64::from_hex("1ad112dac61efb8f"),
    Uint64::from_hex("691bc30ec5faa427"), Uint64::from_hex("ff246311cc327143"),
    Uint64::from_hex("3142368e30e53206"), Uint64::from_hex("71380e31e02ca396"),
    Uint64::from_hex("958d5c960aad76f1"), Uint64::from_hex("f8d6f430c16da536"),
    Uint64::from_hex("c8ffd13f1be7e1d2"), Uint64::from_hex("7578ae66004ddbe1"),
    Uint64::from_hex("05833f01067be646"), Uint64::from_hex("bb34b5ad3bfe586d"),
    Uint64::from_hex("095f34c9a12b97f0"), Uint64::from_hex("247ab64525d60ca8"),
    Uint64::from_hex("dcdbc6f3017477d1"), Uint64::from_hex("4a2e14d4decad24d"),
    Uint64::from_hex("bdb5e6d9be0a1eeb"), Uint64::from_hex("2a7e70f7794301ab"),
    Uint64::from_hex("def42d8a270540fd"), Uint64::from_hex("01078ec0a34c22c1"),
    Uint64::from_hex("e5de511af4c16387"), Uint64::from_hex("7ebb3a52bd9a330a"),
    Uint64::from_hex("77697857aa7d6435"), Uint64::from_hex("004e831603ae4c32"),
    Uint64::from_hex("e7a21020ad78e312"), Uint64::from_hex("9d41a70c6ab420f2"),
    Uint64::from_hex("28e06c18ea1141e6"), Uint64::from_hex("d2b28cbd984f6b28"),
    Uint64::from_hex("26b75f6c446e9d83"), Uint64::from_hex("ba47568c4d418d7f"),
    Uint64::from_hex("d80badbfe6183d8e"), Uint64::from_hex("0e206d7f5f166044"),
    Uint64::from_hex("e258a43911cbca3e"), Uint64::from_hex("723a1746b21dc0bc"),
    Uint64::from_hex("c7caa854f5d7cdd3"), Uint64::from_hex("7cac32883d261d9c"),
    Uint64::from_hex("7690c26423ba942c"), Uint64::from_hex("17e55524478042b8"),
    Uint64::from_hex("e0be477656a2389f"), Uint64::from_hex("4d289b5e67ab2da0"),
    Uint64::from_hex("44862b9c8fbbfd31"), Uint64::from_hex("b47cc8049d141365"),
    Uint64::from_hex("822c1b362b91c793"), Uint64::from_hex("4eb14655fb13dfd8"),
    Uint64::from_hex("1ecbba0714e2a97b"), Uint64::from_hex("6143459d5cde5f14"),
    Uint64::from_hex("53a8fbf1d5f0ac89"), Uint64::from_hex("97ea04d81c5e5b00"),
    Uint64::from_hex("622181a8d4fdb3f3"), Uint64::from_hex("e9bcd341572a1208"),
    Uint64::from_hex("1411258643cce58a"), Uint64::from_hex("9144c5fea4c6e0a4"),
    Uint64::from_hex("0d33d06565cf620f"), Uint64::from_hex("54a48d489f219ca1"),
    Uint64::from_hex("c43e5eac6d63c821"), Uint64::from_hex("a9728b3a72770daf"),
    Uint64::from_hex("d7934e7b20df87ef"), Uint64::from_hex("e35503b61a3e86e5"),
    Uint64::from_hex("cae321fbc819d504"), Uint64::from_hex("129a50b3ac60bfa6"),
    Uint64::from_hex("cd5e68ea7e9fb6c3"), Uint64::from_hex("b01c90199483b1c7"),
    Uint64::from_hex("3de93cd5c295376c"), Uint64::from_hex("aed52edf2ab9ad13"),
    Uint64::from_hex("2e60f512c0a07884"), Uint64::from_hex("bc3d86a3e36210c9"),
    Uint64::from_hex("35269d9b163951ce"), Uint64::from_hex("0c7d6e2ad0cdb5fa"),
    Uint64::from_hex("59e86297d87f5733"), Uint64::from_hex("298ef221898db0e7"),
    Uint64::from_hex("55000029d1a5aa7e"), Uint64::from_hex("8bc08ae1b5061b45"),
    Uint64::from_hex("c2c31c2b6c92703a"), Uint64::from_hex("94cc596baf25ef42"),
    Uint64::from_hex("0a1d73db22540456"), Uint64::from_hex("04b6a0f9d9c4179a"),
    Uint64::from_hex("effdafa2ae3d3c60"), Uint64::from_hex("f7c8075bb49496c4"),
    Uint64::from_hex("9cc5c7141d1cd4e3"), Uint64::from_hex("78bd1638218e5534"),
    Uint64::from_hex("b2f11568f850246a"), Uint64::from_hex("edfabcfa9502bc29"),
    Uint64::from_hex("796ce5f2da23051b"), Uint64::from_hex("aae128b0dc93537c"),
    Uint64::from_hex("3a493da0ee4b29ae"), Uint64::from_hex("b5df6b2c416895d7"),
    Uint64::from_hex("fcabbd25122d7f37"), Uint64::from_hex("70810b58105dc4b1"),
    Uint64::from_hex("e10fdd37f7882a90"), Uint64::from_hex("524dcab5518a3f5c"),
    Uint64::from_hex("3c9e85878451255b"), Uint64::from_hex("4029828119bd34e2"),
    Uint64::from_hex("74a05b6f5d3ceccb"), Uint64::from_hex("b610021542e13eca"),
    Uint64::from_hex("0ff979d12f59e2ac"), Uint64::from_hex("6037da27e4f9cc50"),
    Uint64::from_hex("5e92975a0df1847d"), Uint64::from_hex("d66de190d3e623fe"),
    Uint64::from_hex("5032d6b87b568048"), Uint64::from_hex("9a36b7ce8235216e"),
    Uint64::from_hex("80272a7a24f64b4a"), Uint64::from_hex("93efed8b8c6916f7"),
    Uint64::from_hex("37ddbff44cce1555"), Uint64::from_hex("4b95db5d4b99bd25"),
    Uint64::from_hex("92d3fda169812fc0"), Uint64::from_hex("fb1a4a9a90660bb6"),
    Uint64::from_hex("730c196946a4b9b2"), Uint64::from_hex("81e289aa7f49da68"),
    Uint64::from_hex("64669a0f83b1a05f"), Uint64::from_hex("27b3ff7d9644f48b"),
    Uint64::from_hex("cc6b615c8db675b3"), Uint64::from_hex("674f20b9bcebbe95"),
    Uint64::from_hex("6f31238275655982"), Uint64::from_hex("5ae488713e45cf05"),
    Uint64::from_hex("bf619f9954c21157"), Uint64::from_hex("eabac46040a8eae9"),
    Uint64::from_hex("454c6fe9f2c0c1cd"), Uint64::from_hex("419cf6496412691c"),
    Uint64::from_hex("d3dc3bef265b0f70"), Uint64::from_hex("6d0e60f5c3578a9e")
);

$tiger_t4 = array (
    Uint64::from_hex("5b0e608526323c55"), Uint64::from_hex("1a46c1a9fa1b59f5"),
    Uint64::from_hex("a9e245a17c4c8ffa"), Uint64::from_hex("65ca5159db2955d7"),
    Uint64::from_hex("05db0a76ce35afc2"), Uint64::from_hex("81eac77ea9113d45"),
    Uint64::from_hex("528ef88ab6ac0a0d"), Uint64::from_hex("a09ea253597be3ff"),
    Uint64::from_hex("430ddfb3ac48cd56"), Uint64::from_hex("c4b3a67af45ce46f"),
    Uint64::from_hex("4ececfd8fbe2d05e"), Uint64::from_hex("3ef56f10b39935f0"),
    Uint64::from_hex("0b22d6829cd619c6"), Uint64::from_hex("17fd460a74df2069"),
    Uint64::from_hex("6cf8cc8e8510ed40"), Uint64::from_hex("d6c824bf3a6ecaa7"),
    Uint64::from_hex("61243d581a817049"), Uint64::from_hex("048bacb6bbc163a2"),
    Uint64::from_hex("d9a38ac27d44cc32"), Uint64::from_hex("7fddff5baaf410ab"),
    Uint64::from_hex("ad6d495aa804824b"), Uint64::from_hex("e1a6a74f2d8c9f94"),
    Uint64::from_hex("d4f7851235dee8e3"), Uint64::from_hex("fd4b7f886540d893"),
    Uint64::from_hex("247c20042aa4bfda"), Uint64::from_hex("096ea1c517d1327c"),
    Uint64::from_hex("d56966b4361a6685"), Uint64::from_hex("277da5c31221057d"),
    Uint64::from_hex("94d59893a43acff7"), Uint64::from_hex("64f0c51ccdc02281"),
    Uint64::from_hex("3d33bcc4ff6189db"), Uint64::from_hex("e005cb184ce66af1"),
    Uint64::from_hex("ff5ccd1d1db99bea"), Uint64::from_hex("b0b854a7fe42980f"),
    Uint64::from_hex("7bd46a6a718d4b9f"), Uint64::from_hex("d10fa8cc22a5fd8c"),
    Uint64::from_hex("d31484952be4bd31"), Uint64::from_hex("c7fa975fcb243847"),
    Uint64::from_hex("4886ed1e5846c407"), Uint64::from_hex("28cddb791eb70b04"),
    Uint64::from_hex("c2b00be2f573417f"), Uint64::from_hex("5c9590452180f877"),
    Uint64::from_hex("7a6bddfff370eb00"), Uint64::from_hex("ce509e38d6d9d6a4"),
    Uint64::from_hex("ebeb0f00647fa702"), Uint64::from_hex("1dcc06cf76606f06"),
    Uint64::from_hex("e4d9f28ba286ff0a"), Uint64::from_hex("d85a305dc918c262"),
    Uint64::from_hex("475b1d8732225f54"), Uint64::from_hex("2d4fb51668ccb5fe"),
    Uint64::from_hex("a679b9d9d72bba20"), Uint64::from_hex("53841c0d912d43a5"),
    Uint64::from_hex("3b7eaa48bf12a4e8"), Uint64::from_hex("781e0e47f22f1ddf"),
    Uint64::from_hex("eff20ce60ab50973"), Uint64::from_hex("20d261d19dffb742"),
    Uint64::from_hex("16a12b03062a2e39"), Uint64::from_hex("1960eb2239650495"),
    Uint64::from_hex("251c16fed50eb8b8"), Uint64::from_hex("9ac0c330f826016e"),
    Uint64::from_hex("ed152665953e7671"), Uint64::from_hex("02d63194a6369570"),
    Uint64::from_hex("5074f08394b1c987"), Uint64::from_hex("70ba598c90b25ce1"),
    Uint64::from_hex("794a15810b9742f6"), Uint64::from_hex("0d5925e9fcaf8c6c"),
    Uint64::from_hex("3067716cd868744e"), Uint64::from_hex("910ab077e8d7731b"),
    Uint64::from_hex("6a61bbdb5ac42f61"), Uint64::from_hex("93513efbf0851567"),
    Uint64::from_hex("f494724b9e83e9d5"), Uint64::from_hex("e887e1985c09648d"),
    Uint64::from_hex("34b1d3c675370cfd"), Uint64::from_hex("dc35e433bc0d255d"),
    Uint64::from_hex("d0aab84234131be0"), Uint64::from_hex("08042a50b48b7eaf"),
    Uint64::from_hex("9997c4ee44a3ab35"), Uint64::from_hex("829a7b49201799d0"),
    Uint64::from_hex("263b8307b7c54441"), Uint64::from_hex("752f95f4fd6a6ca6"),
    Uint64::from_hex("927217402c08c6e5"), Uint64::from_hex("2a8ab754a795d9ee"),
    Uint64::from_hex("a442f7552f72943d"), Uint64::from_hex("2c31334e19781208"),
    Uint64::from_hex("4fa98d7ceaee6291"), Uint64::from_hex("55c3862f665db309"),
    Uint64::from_hex("bd0610175d53b1f3"), Uint64::from_hex("46fe6cb840413f27"),
    Uint64::from_hex("3fe03792df0cfa59"), Uint64::from_hex("cfe700372eb85e8f"),
    Uint64::from_hex("a7be29e7adbce118"), Uint64::from_hex("e544ee5cde8431dd"),
    Uint64::from_hex("8a781b1b41f1873e"), Uint64::from_hex("a5c94c78a0d2f0e7"),
    Uint64::from_hex("39412e2877b60728"), Uint64::from_hex("a1265ef3afc9a62c"),
    Uint64::from_hex("bcc2770c6a2506c5"), Uint64::from_hex("3ab66dd5dce1ce12"),
    Uint64::from_hex("e65499d04a675b37"), Uint64::from_hex("7d8f523481bfd216"),
    Uint64::from_hex("0f6f64fcec15f389"), Uint64::from_hex("74efbe618b5b13c8"),
    Uint64::from_hex("acdc82b714273e1d"), Uint64::from_hex("dd40bfe003199d17"),
    Uint64::from_hex("37e99257e7e061f8"), Uint64::from_hex("fa52626904775aaa"),
    Uint64::from_hex("8bbbf63a463d56f9"), Uint64::from_hex("f0013f1543a26e64"),
    Uint64::from_hex("a8307e9f879ec898"), Uint64::from_hex("cc4c27a4150177cc"),
    Uint64::from_hex("1b432f2cca1d3348"), Uint64::from_hex("de1d1f8f9f6fa013"),
    Uint64::from_hex("606602a047a7ddd6"), Uint64::from_hex("d237ab64cc1cb2c7"),
    Uint64::from_hex("9b938e7225fcd1d3"), Uint64::from_hex("ec4e03708e0ff476"),
    Uint64::from_hex("feb2fbda3d03c12d"), Uint64::from_hex("ae0bced2ee43889a"),
    Uint64::from_hex("22cb8923ebfb4f43"), Uint64::from_hex("69360d013cf7396d"),
    Uint64::from_hex("855e3602d2d4e022"), Uint64::from_hex("073805bad01f784c"),
    Uint64::from_hex("33e17a133852f546"), Uint64::from_hex("df4874058ac7b638"),
    Uint64::from_hex("ba92b29c678aa14a"), Uint64::from_hex("0ce89fc76cfaadcd"),
    Uint64::from_hex("5f9d4e0908339e34"), Uint64::from_hex("f1afe9291f5923b9"),
    Uint64::from_hex("6e3480f60f4a265f"), Uint64::from_hex("eebf3a2ab29b841c"),
    Uint64::from_hex("e21938a88f91b4ad"), Uint64::from_hex("57dfeff845c6d3c3"),
    Uint64::from_hex("2f006b0bf62caaf2"), Uint64::from_hex("62f479ef6f75ee78"),
    Uint64::from_hex("11a55ad41c8916a9"), Uint64::from_hex("f229d29084fed453"),
    Uint64::from_hex("42f1c27b16b000e6"), Uint64::from_hex("2b1f76749823c074"),
    Uint64::from_hex("4b76eca3c2745360"), Uint64::from_hex("8c98f463b91691bd"),
    Uint64::from_hex("14bcc93cf1ade66a"), Uint64::from_hex("8885213e6d458397"),
    Uint64::from_hex("8e177df0274d4711"), Uint64::from_hex("b49b73b5503f2951"),
    Uint64::from_hex("10168168c3f96b6b"), Uint64::from_hex("0e3d963b63cab0ae"),
    Uint64::from_hex("8dfc4b5655a1db14"), Uint64::from_hex("f789f1356e14de5c"),
    Uint64::from_hex("683e68af4e51dac1"), Uint64::from_hex("c9a84f9d8d4b0fd9"),
    Uint64::from_hex("3691e03f52a0f9d1"), Uint64::from_hex("5ed86e46e1878e80"),
    Uint64::from_hex("3c711a0e99d07150"), Uint64::from_hex("5a0865b20c4e9310"),
    Uint64::from_hex("56fbfc1fe4f0682e"), Uint64::from_hex("ea8d5de3105edf9b"),
    Uint64::from_hex("71abfdb12379187a"), Uint64::from_hex("2eb99de1bee77b9c"),
    Uint64::from_hex("21ecc0ea33cf4523"), Uint64::from_hex("59a4d7521805c7a1"),
    Uint64::from_hex("3896f5eb56ae7c72"), Uint64::from_hex("aa638f3db18f75dc"),
    Uint64::from_hex("9f39358dabe9808e"), Uint64::from_hex("b7defa91c00b72ac"),
    Uint64::from_hex("6b5541fd62492d92"), Uint64::from_hex("6dc6dee8f92e4d5b"),
    Uint64::from_hex("353f57abc4beea7e"), Uint64::from_hex("735769d6da5690ce"),
    Uint64::from_hex("0a234aa642391484"), Uint64::from_hex("f6f9508028f80d9d"),
    Uint64::from_hex("b8e319a27ab3f215"), Uint64::from_hex("31ad9c1151341a4d"),
    Uint64::from_hex("773c22a57bef5805"), Uint64::from_hex("45c7561a07968633"),
    Uint64::from_hex("f913da9e249dbe36"), Uint64::from_hex("da652d9b78a64c68"),
    Uint64::from_hex("4c27a97f3bc334ef"), Uint64::from_hex("76621220e66b17f4"),
    Uint64::from_hex("967743899acd7d0b"), Uint64::from_hex("f3ee5bcae0ed6782"),
    Uint64::from_hex("409f753600c879fc"), Uint64::from_hex("06d09a39b5926db6"),
    Uint64::from_hex("6f83aeb0317ac588"), Uint64::from_hex("01e6ca4a86381f21"),
    Uint64::from_hex("66ff3462d19f3025"), Uint64::from_hex("72207c24ddfd3bfb"),
    Uint64::from_hex("4af6b6d3e2ece2eb"), Uint64::from_hex("9c994dbec7ea08de"),
    Uint64::from_hex("49ace597b09a8bc4"), Uint64::from_hex("b38c4766cf0797ba"),
    Uint64::from_hex("131b9373c57c2a75"), Uint64::from_hex("b1822cce61931e58"),
    Uint64::from_hex("9d7555b909ba1c0c"), Uint64::from_hex("127fafdd937d11d2"),
    Uint64::from_hex("29da3badc66d92e4"), Uint64::from_hex("a2c1d57154c2ecbc"),
    Uint64::from_hex("58c5134d82f6fe24"), Uint64::from_hex("1c3ae3515b62274f"),
    Uint64::from_hex("e907c82e01cb8126"), Uint64::from_hex("f8ed091913e37fcb"),
    Uint64::from_hex("3249d8f9c80046c9"), Uint64::from_hex("80cf9bede388fb63"),
    Uint64::from_hex("1881539a116cf19e"), Uint64::from_hex("5103f3f76bd52457"),
    Uint64::from_hex("15b7e6f5ae47f7a8"), Uint64::from_hex("dbd7c6ded47e9ccf"),
    Uint64::from_hex("44e55c410228bb1a"), Uint64::from_hex("b647d4255edb4e99"),
    Uint64::from_hex("5d11882bb8aafc30"), Uint64::from_hex("f5098bbb29d3212a"),
    Uint64::from_hex("8fb5ea14e90296b3"), Uint64::from_hex("677b942157dd025a"),
    Uint64::from_hex("fb58e7c0a390acb5"), Uint64::from_hex("89d3674c83bd4a01"),
    Uint64::from_hex("9e2da4df4bf3b93b"), Uint64::from_hex("fcc41e328cab4829"),
    Uint64::from_hex("03f38c96ba582c52"), Uint64::from_hex("cad1bdbd7fd85db2"),
    Uint64::from_hex("bbb442c16082ae83"), Uint64::from_hex("b95fe86ba5da9ab0"),
    Uint64::from_hex("b22e04673771a93f"), Uint64::from_hex("845358c9493152d8"),
    Uint64::from_hex("be2a488697b4541e"), Uint64::from_hex("95a2dc2dd38e6966"),
    Uint64::from_hex("c02c11ac923c852b"), Uint64::from_hex("2388b1990df2a87b"),
    Uint64::from_hex("7c8008fa1b4f37be"), Uint64::from_hex("1f70d0c84d54e503"),
    Uint64::from_hex("5490adec7ece57d4"), Uint64::from_hex("002b3c27d9063a3a"),
    Uint64::from_hex("7eaea3848030a2bf"), Uint64::from_hex("c602326ded2003c0"),
    Uint64::from_hex("83a7287d69a94086"), Uint64::from_hex("c57a5fcb30f57a8a"),
    Uint64::from_hex("b56844e479ebe779"), Uint64::from_hex("a373b40f05dcbce9"),
    Uint64::from_hex("d71a786e88570ee2"), Uint64::from_hex("879cbacdbde8f6a0"),
    Uint64::from_hex("976ad1bcc164a32f"), Uint64::from_hex("ab21e25e9666d78b"),
    Uint64::from_hex("901063aae5e5c33c"), Uint64::from_hex("9818b34448698d90"),
    Uint64::from_hex("e36487ae3e1e8abb"), Uint64::from_hex("afbdf931893bdcb4"),
    Uint64::from_hex("6345a0dc5fbbd519"), Uint64::from_hex("8628fe269b9465ca"),
    Uint64::from_hex("1e5d01603f9c51ec"), Uint64::from_hex("4de44006a15049b7"),
    Uint64::from_hex("bf6c70e5f776cbb1"), Uint64::from_hex("411218f2ef552bed"),
    Uint64::from_hex("cb0c0708705a36a3"), Uint64::from_hex("e74d14754f986044"),
    Uint64::from_hex("cd56d9430ea8280e"), Uint64::from_hex("c12591d7535f5065"),
    Uint64::from_hex("c83223f1720aef96"), Uint64::from_hex("c3a0396f7363a51f")
);


function tiger_round(Uint64 &$a, Uint64 &$b, Uint64 &$c, Uint64 $ix, $mulf) {
    global $tiger_t1, $tiger_t2, $tiger_t3, $tiger_t4;
    $c->xore($ix);
    $t = $tiger_t1[$c->gb(0)]->xor_($tiger_t2[$c->gb(2)]);
    $t->xore($tiger_t3[$c->gb(4)]);
    $t->xore($tiger_t4[$c->gb(6)]);
    $a->sube($t);
    $t = $tiger_t4[$c->gb(1)]->xor_($tiger_t3[$c->gb(3)]);
    $t->xore($tiger_t2[$c->gb(5)]);
    $t->xore($tiger_t1[$c->gb(7)]);
    $b->adde($t);
    $mulf($b);
}

function tiger_pass(Uint64 &$a,Uint64 &$b,Uint64 &$c, array $is, $mulf) {
    tiger_round($a,$b,$c,$is[0], $mulf);
    tiger_round($b,$c,$a,$is[1], $mulf);
    tiger_round($c,$a,$b,$is[2], $mulf);
    tiger_round($a,$b,$c,$is[3], $mulf);
    tiger_round($b,$c,$a,$is[4], $mulf);
    tiger_round($c,$a,$b,$is[5], $mulf);
    tiger_round($a,$b,$c,$is[6], $mulf);
    tiger_round($b,$c,$a,$is[7], $mulf);
}


$tiger_c1 = Uint64::from_hex("A5A5A5A5A5A5A5A5");
$tiger_c2 = Uint64::from_hex("0123456789ABCDEF");

function tiger_key_sched(array $is) {
    global $tiger_c1, $tiger_c2;
    $is[0]->sube($is[7]->xor_($tiger_c1));
    $is[1]->xore($is[0]);
    $is[2]->adde($is[1]);
    $is[3]->sube($is[1]->not_()->shlie(19)->xore($is[2]));
    $is[4]->xore($is[3]);
    $is[5]->adde($is[4]);
    $is[6]->sube($is[4]->not_()->shrie(23)->xore($is[5]));
    $is[7]->xore($is[6]);
    $is[0]->adde($is[7]);
    $is[1]->sube($is[7]->not_()->shlie(19)->xore($is[0]));
    $is[2]->xore($is[1]);
    $is[3]->adde($is[2]);
    $is[4]->sube($is[2]->not_()->shrie(23)->xore($is[3]));
    $is[5]->xore($is[4]);
    $is[6]->adde($is[5]);
    $is[7]->sube($is[6]->xor_($tiger_c2));
    return $is;
}

function tiger_block(array $is, array $res) {
    $mulf5 = function(Uint64 &$m) {
        $m=$m->shli(2)->adde($m);
    };
    $mulf7 = function(Uint64 &$m) {
        $m=$m->shli(3)->sube($m);
    };
    $mulf9 = function(Uint64 &$m) {
        $m=$m->shli(3)->adde($m);
    };
    
    $r0 = clone $res[0];
    $r1 = clone $res[1];
    $r2 = clone $res[2];
    tiger_pass($r0,$r1,$r2,$is,$mulf5);
    $is = tiger_key_sched($is);
    tiger_pass($r2,$r0,$r1,$is,$mulf7);
    $is = tiger_key_sched($is);
    tiger_pass($r1,$r2,$r0,$is,$mulf9);
    $r0->xore($res[0]);
    $r1->sube($res[1]);
    $r2->adde($res[2]);
    return array($r0,$r1,$r2);
}

//strlen($is) must be <= 63
function tiger_end($is, Uint64 $tlen, array $res) {
    $length = strlen($is);
    $is .= "\001";
    while (strlen($is) % 8)
        $is .= "\000";
    if(strlen($is) == 64) {
        $res = tiger_block(Uint64::arr_from_les($is), $res);
        $is = "";
    }
    while (strlen($is) < 56)
        $is .= "\000";
    $arr = Uint64::arr_from_les($is);
    $arr[7] = $tlen->shli(3);
    $res = tiger_block($arr, $res);
    return $res;
}

/** Return the hash of the desired string as 3 Uint64s **/
function tiger($str)
{
    $length=strlen($str);
    //Process the data
    $bs=str_split($str,64);
    $lb=array_pop($bs);
    if(strlen($lb) == 64) {
        array_push($bs,$lb);
        $lb="";
    }
    return tiger_end($lb, Uint64::from_int($length),array_reduce($bs,
        function($v, $w) {
            return tiger_block(Uint64::arr_from_les($w),$v);
        },
        array(Uint64::from_hex("0123456789ABCDEF"),
              Uint64::from_hex("FEDCBA9876543210"),
              Uint64::from_hex("F096A5B4C3B2E187")
        )
    ));
}

/** Return the data that needs to be stored to call tigerp2 **/
function tigerp1($password, $salt)
{
    $hs = strlen($password);
    $n = 128-($hs%64);
    $data = $password . substr($salt,0,$n);
    $hs = $hs + $n;
    //Process the data
    $bs=str_split($data,64);
    $h = array_reduce($bs,
        function($v, $w) {
            return tiger_block(Uint64::arr_from_les($w),$v);
        },
        array(Uint64::from_hex("0123456789ABCDEF"),
              Uint64::from_hex("FEDCBA9876543210"),
              Uint64::from_hex("F096A5B4C3B2E187")
        )
    );
    return array( 'h' => $h, 'r' => $salt, 'n' => $n, 'hs' => $hs);
}

/** end processing the request started in tigerp1, adding any extra data**/
function tigerp2($h, $r, $n, $hs, $s)
{
    $data = substr($r,$n) . $s;
    $thd = $hs + strlen($data);

    //Process the data
    $bs=str_split($data,64);
    $lb=array_pop($bs);
    if(strlen($lb) == 64) {
        array_push($bs,$lb);
        $lb="";
    }
    return tiger_end($lb, Uint64::from_int($thd),array_reduce($bs,
        function($v, $w) {
            return tiger_block(Uint64::arr_from_les($w),$v);
        },$h));
}


// /*Example code*/
// function t_hash($str) {
//     foreach (tiger($str) as $v)
//         for ($i= 0; $i <8; $i++)
//             printf("%02X",$v->gb($i));
//     echo "\n";
//     t_hash2($str);
// }
// 
// //Example using tigerp1 and tigerp2
// function t_hash2($str) {
//     $psalt=openssl_random_pseudo_bytes (128, $strong);
//     //You should assure strong is true here
//     $s1 = tigerp1($str,$psalt);
//     //s1 contains all the data we need to store
//     //tsalt is the salt used once for protocol run
//     for ($i= 0; $i < 100; $i++) {
//         $tsalt=openssl_random_pseudo_bytes (32, $strong);
//         //You should assure strong is true here
//         $ress = tigerp2($s1['h'],$s1['r'],$s1['n'],$s1['hs'],$tsalt);
//         $resc = tiger($str . $psalt . $tsalt);
//         if ($ress != $resc) {
//             echo "Error calculating partial hashes!\n";
//         }
//     }
// }
// 
// t_hash("");
// t_hash("abc");
// t_hash("Tiger");
// /* Hash of 512-bit strings */
// t_hash("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-");
// t_hash("ABCDEFGHIJKLMNOPQRSTUVWXYZ=abcdefghijklmnopqrstuvwxyz+0123456789");
// t_hash("Tiger - A Fast New Hash Function, by Ross Anderson and Eli Biham");
// /* Hash of two-block strings strings */
// t_hash("Tiger - A Fast New Hash Function, by Ross Anderson and Eli Biham, proceedings of Fast Software Encryption 3, Cambridge.");
// t_hash("Tiger - A Fast New Hash Function, by Ross Anderson and Eli Biham, proceedings of Fast Software Encryption 3, Cambridge, 1996.");
// t_hash("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-");

?>
