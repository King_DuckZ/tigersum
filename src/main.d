/*	tigersum - a command line utility to calculate the tiger hash from input.
	Copyright (C) 2014  Michele "King_DuckZ" Santullo

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import std.stdio;
import std.file;
import std.array;
import std.getopt;
import sums;

mixin(import("tigersumConfig.d"));

int main (string[] parArgs) {
	if (parArgs.length == 1)
		return 0;

	const auto cwd = getcwd();
	const auto working_dir = cwd ~ (cwd[$-1] == '/' ? "" : "/");

	bool show_version, show_help, show_license;
	bool check_mode;
	string file_mode = "r";
	bool bsd_style;
	bool switch_quiet, switch_status, switch_warn;
	ushort tiger_version = TIGER_VERSION_DEF;

	void fileModeHandler (string parOption) {
		if (parOption == "text|t") {
			file_mode = "r";
		}
		else if (parOption == "binary|b") {
			file_mode = "rb";
		}
	}

	string[] args = parArgs[];
	getopt(args,
		std.getopt.config.passThrough,
		std.getopt.config.bundling,
		"version|v", &show_version,
		"help|h", &show_help,
		"check|c", &check_mode,
		"tiger|i", &tiger_version,
		"licence|license", &show_license,
		"binary|b", &fileModeHandler,
		"text|t", &fileModeHandler,
		"tag", &bsd_style
		//"quiet", &switch_quiet,
		//"status", &switch_status,
		//"warn|w", &switch_warn
	);

	if (check_mode) {
		checkSums(args[1 .. $], working_dir, tiger_version == 2, file_mode);
	}
	else if (show_help) {
		writefln("Usage: %s [OPTION]... [FILE]...", PROGRAM_NAME);
		writeln("  -b, --binary        read in binary mode");
		writeln("  -c, --check         read tiger sums from FILEs and check them");
		writeln("      --tag           create a BSD-style checksum");
		writeln("  -t, --text          read in text mode (default)");
		writefln("  -i n, --tiger=n     set the tiger version that should be used; valid values are 1 or 2, %d is default", TIGER_VERSION_DEF);
		writeln();
		//writeln("The following three options are useful only when verifying checksums:");
		//writeln("      --quiet         don't print OK for each successfully verified file");
		//writeln("      --status        don't output anything, status code shows success");
		//writeln("  -w, --warn          warn about improperly formatted checksum lines");
		writeln();
		writeln("  -h, --help          display this help and exit");
		writeln("  -v, --version       display version informations and exit");
		writefln("      --license       display details about the license under which %s is distributed", PROGRAM_NAME);
		writeln();
		writefln("The sums are computed using the %s backend. When checking, the input", BACKEND_NAME);
		writeln("should be a former output of this program. The default mode is to print");
		writeln("a line with checksum, a character indicating type(`*' for binary, ` ' for");
		writeln("text), and name for each FILE.");
		writeln();
		writeln("Report bugs on <https://bitbucket.org/King_DuckZ/tigersum/issues?status=new&status=open>");
		writeln("tigersum home page: <https://bitbucket.org/King_DuckZ/tigersum>");
		writefln("%s home page: <%s>", BACKEND_NAME, BACKEND_WEBSITE);
	}
	else if (show_version) {
		writefln("%s %d.%d Copyright © %d  Michele \"King_DuckZ\" Santullo", PROGRAM_NAME, TIGERSUM_VERSION_MAJOR, TIGERSUM_VERSION_MINOR, COPYRIGHT_YEAR);
		writefln("Using the %s implementation by %s", BACKEND_NAME, BACKEND_AUTHOR);
		writeln(BACKEND_WEBSITE);
		writeln();
		writeln("This program comes with ABSOLUTELY NO WARRANTY.");
		writeln("This is free software, and you are welcome to redistribute it");
		writefln("under certain conditions; run %s --license for details.", PROGRAM_NAME);
	}
	else if (show_license) {
		writefln("%s - a command line utility to calculate the tiger hash from input.", PROGRAM_NAME);
		writefln("Copyright © %d  Michele \"King_DuckZ\" Santullo", COPYRIGHT_YEAR);
		writeln();
		writefln("%s is free software: you can redistribute it and/or modify", PROGRAM_NAME);
		writeln("it under the terms of the GNU General Public License as published by");
		writeln("the Free Software Foundation, either version 3 of the License, or");
		writeln("(at your option) any later version.");
		writeln();
		writefln("%s is distributed in the hope that it will be useful,", PROGRAM_NAME);
		writeln("but WITHOUT ANY WARRANTY; without even the implied warranty of");
		writeln("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
		writeln("GNU General Public License for more details.");
		writeln();
		writeln("You should have received a copy of the GNU General Public License");
		writeln("along with this program.  If not, see <http://www.gnu.org/licenses/>.");
	}
	else {
		if (tiger_version != 1 && tiger_version != 2) {
			writefln("Invalid tiger version requested: %d - use %s --help for help on program's usage.", tiger_version, PROGRAM_NAME);
			return 2;
		}
		calculateSums(args[1 .. $], working_dir, tiger_version == 2, file_mode, bsd_style);
	}

	return 0;
}
